Haar-like features are simple and inexpensive image features based on intensity differences between adjacent rectangular regions. 
Properties:
1- High inter-class variability
2- Low intra-class variability
3- Local oriented intensity differences
4- Different scales
5- Computationally efficient

Cascade classifier of haar-like features:
A strong classifier comprises of series of WEAK CLASSIFIERS (normally more than 10), and a weak classifier itselfs includes a set of a few haar-like features(normally 2 to 5)

Training:
The process of selecting appropriate haar-like features for each weak classifier, and then for hole strong classifier constitutes TRAINING PHASE which can be done using a machine learning technique.